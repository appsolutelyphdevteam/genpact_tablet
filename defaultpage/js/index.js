var amountArray = [];

$(function(){
	$body = $("body");

	updateTabletLocation();
	// getBadgeCount();
	addEventListeners();

});

function addEventListeners(){ 
	// LOGIN
	$('#loginBtn').on('click', function(){
		nativeInterface.cashierLogin();
	});

	// HOME
	$('#settingsBtn').on('click', function(){ nativeInterface.settings(); });
	$('#syncWrapper').on('click', function(){ nativeInterface.syncLocallyStoreData("pointtable") });
	$('#logoutBtn').on('click', function(){
		$('#home').removeClass('active-view');
		$('#login').addClass('active-view');
	});

	// REDEEM
	$('#redeemBtn').on('click', function(){ nativeInterface.scan("redeem"); }); 

		$('#earn .logoutBtn2, #redeem .logoutBtn2').on('click', function(){
			$('#redeem').removeClass('active-view');
			$('#earn').removeClass('active-view');
			$('#home').addClass('active-view');
		});

		$('#redeemPts').on('click', function(){ enterAmount('redeem'); });

		$('#voucherList ul').on('click', voucherRedeem);

		$('#empty_state').on('click',function(){
			$('#empty_state').removeClass('active-page');
			nativeInterface.getVoucherList(true);
		});

	// EARN
	$('#earnBtn').on('click', function(){ nativeInterface.scan("earn"); }); 

		$('#earn .container ul').on('click', enterAmountwithCat);

	// MODAL
	$('#prompt #ok').on('click', function(){
		$('#modal').removeClass('prompt').removeClass('active-modal');
	});

	$('#modal #close').on('click', function(){
		$('#modal').removeClass('withPoints');
		$('#modal').removeClass('summary');
		$('#modal').removeClass('prompt');
		$('#modal').removeClass('choice');
		$('#modal').removeClass('input');
		$('#modal').removeClass('active-modal');
	});
}

// function getBadgeCount(){
// 	var count = nativeInterface.getEarnedCount("pointtable");
// 	// var count = 3; //Test
// 	if (typeof count != " undefined" && count != 0 && count != null && count != "") {
// 		document.querySelector('#syncBtn').setAttribute('data-rec', count);
// 		$('#syncBtn').addClass('active-sync');
// 	}else{
// 		document.querySelector('#syncBtn').setAttribute('data-rec', 0);
// 		$('#syncBtn').prop('display', true);
// 		$('#syncBtn').removeClass('active-sync');
// 	}
// }

function updateTabletLocation(){
	var location = nativeInterface.getTabletAssignBranch();
	$('#locationName').html(location);
}


function cashierLoginCallback(){
	$('#login').removeClass('active-view');
	$('#home').addClass('active-view');
}

function scanCallback(type, data){
	if (type == "earn") {
		enterAmount('earn', data);
	} else if (type == "redeem"){
		showRedeem(data)
	}
}

function enterAmount(type, data){
	$('#earn #earnBtn').off('click');
	$('#modal #submitBtn').off('click');
	$('#submitBtn').off('click');
	$('#modal #close').off('click');

	$('#transactionId input').val('');
	$('#amount input').val('');

	if(type=='earn'){

		if (data) {
			if(data.length > 1){
				
				amountArray = [];

				$('#home').removeClass('active-view');
				$('#earn').addClass('active-view');

				var catList = "";

				amountArray = [];

				for (var i = 0; i < data.length; i++) {
					catList+= '<li class="swiper-slide" data-id="'+data[i].id+'" data-setting="'+data[i].settingID+'" data-name="'+data[i].name+'" data-amount="'+data[i].amount+'">'+
									'<div class="name">'+data[i].name+'</div>'+
						        '</li>';
				}
				$('ul.swiper-wrapper').html(catList);

				var swiper = new Swiper ('.swiper-container', {
				  // Optional parameters
					direction: 'horizontal',
					loop: true,
					slidesPerView: 'auto',
					centeredSlides: true
				})

				$('#earn #earnBtn').on('click', showSummary);
				$('#modal #close').on('click', function(){
					$('#modal').removeClass('summary');
					$('#modal').removeClass('input');
					$('#modal').removeClass('active-modal');
				});

			}else{

				$('#withPoints header p').html('Earn Points');	

				$('#modal').removeClass('input');
				$('#modal').addClass('withPoints');
				$('#modal').addClass('active-modal');

				$('#submitBtn').on('click', function(){
					var transactionID = $('#transactionId input').val();
					var amount = $('#amount input').val();

					var id = data[0].id;
					var settingID = data[0].settingID;
					var name = data[0].name;
					var min = data[0].amount;

					if(transactionID == '' || transactionID == null){
						$("#transactionId input").focus();
						nativeInterface.msgBox('Please enter your Transaction ID','Empty Field!');
					}else if(/^[a-zA-Z0-9- ]*$/.test(transactionID) == false){
						$("#transactionId input").focus();
						nativeInterface.msgBox('Special characters are not allowed','Invalid Input!');
					}else if(isNaN(parseFloat(amount))){
						$("#amount input").focus();
						nativeInterface.msgBox('Amount is invalid!','Invalid Input!');
					}else if(parseFloat(amount) < min){
						$("#input input").focus();
						console.log(amount, min)
						nativeInterface.msgBox('Sorry, you have provided an invalid input. Please enter a number greater than the base amount.','Invalid Input!');
					}else{
						$('#modal').removeClass('withPoints');
						$('#modal').removeClass('active-modal');

						var completeData = {
							transactionID: transactionID,
							transactions: [{
								id: id,
								settingID: settingID,
								name: name,
								amount: parseFloat(amount).toFixed(2)
							}]
						};
						console.log(JSON.stringify(completeData));
						nativeInterface.earn(JSON.stringify(completeData));
					}
				});

				$('#modal #close').on('click', function(){
                    $('#modal').removeClass('summary');
                    $('#modal').removeClass('input');
                    $('#modal').removeClass('active-modal');
                });

			}
		}
	}else{

		$('#transactionId input').val('');
		$('#amount input').val('');
		
		$('#modal').addClass('withPoints').addClass('active-modal');
		$('#amount p').html('Enter Points to Redeem');

		$('#withPoints header p').html('Redeem Points');

		$('#modal #submitBtn').on('click', function(){
			var transactionID = $('#transactionId input').val();
			var amount = $('#amount input').val();

			if(transactionID == '' || transactionID == null){
				$("#transactionId input").focus();
				nativeInterface.msgBox('Please enter your Transaction ID','Empty Field!');
			}else if(/^[a-zA-Z0-9- ]*$/.test(transactionID) == false){
				$("#transactionId input").focus();
				nativeInterface.msgBox('Special characters are not allowed','Invalid Input!');
			}else if(isNaN(parseFloat(amount))){
				$("#amount input").focus();
				nativeInterface.msgBox('Amount is invalid!','Invalid Input!');
			}else{
				$('#modal').removeClass('withPoints');
				$('#modal').removeClass('active-modal');
				confirmRedeem(transactionID, amount); //TEST
			}
		});

		$('#modalContent #close').on('click', function(){
			$('#modal').removeClass('withPoints');
			$('#modal').removeClass('active-modal');
		});
	}
}

function enterAmountwithCat(e,data){
	$('#input footer #submitBtn').off('click');
	$('#summary #add').off('click');

	if (e.target != e.currentTarget) {
		var id = e.target.getAttribute('data-id');
		var setting = e.target.getAttribute('data-setting');
		var name = e.target.getAttribute('data-name');
		var min = parseFloat(e.target.getAttribute('data-amount')).toFixed(2);

		$('#input input').val('');

		$('#modal').removeClass('summary');
		$('#modal').addClass('input');
		$('#modal').addClass('active-modal');

		$('#input #label').html('Enter Amount');
		$('#input input').attr('type', 'number');
		$('#input input').on('keypress', validateInput);

		$('#input footer #submitBtn').on('click', function(){
			
			var categoryAmount = parseFloat($('#input input').val());

			if(isNaN(parseFloat(categoryAmount))){
				$("#input input").focus();
				console.log(categoryAmount)
				nativeInterface.msgBox('Amount is invalid!','Invalid Input!');
			}else if(parseFloat(categoryAmount) < min){
				$("#input input").focus();
				console.log("amount less than min")
				nativeInterface.msgBox('Sorry, you have provided an invalid input. Please enter a number greater than the base amount.','Invalid Input!');
			}else{
				var data = {
					id: id,
        			settingID: setting,
        			name: name,
					amount: parseFloat(categoryAmount).toFixed(2),
				};
				amountArray.push(data);
				$('#inputModal').removeClass('active-modal');
				$('#complete').addClass('display');
				showSummary();
			}

			$('#summary #add').on('click', function(){
				$('#summary').removeClass('active-modal');
			});
		});
	}
}

function showSummary(){
	
	console.log(amountArray);

	if(amountArray == "" || amountArray == null){
		nativeInterface.msgBox("Please select category and enter amount", "Invalid Input!");
	}else{
		$('#continue').off('click');
		var content=""

		$('#modal').removeClass('input');
		$('#modal').removeClass('active-modal');

		$('#modal').addClass('summary').addClass('active-modal');
		$('#summary ul.earnList').html(content);

		for (var i = 0; i < amountArray.length; i++) {
			content+= 	'<li>'+ amountArray[i].name + '</li>'+
						'<li>'+ amountArray[i].amount + '</li>';
		}

		$('#summary ul.earnList').html(content);

		$('#summary #submitBtn').on('click', function(){

			var transactionID = $('#transactionID input').val();
			var specialExp = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;

			if(transactionID == '' || transactionID == null){
				$('#transactionID input').focus();
				nativeInterface.msgBox("Please enter Transaction ID", "Invalid Input!");
			}else if(/^[a-zA-Z0-9]*$/.test(transactionID) == false){
				$('#transactionID input').focus();
				nativeInterface.msgBox("Special characters are not allowed", "Invalid Input!");
			}else{
				$('#summary').removeClass('active-modal');

				var completeData = {
					transactionID: transactionID,
					transactions: amountArray,
				};
				console.log(JSON.stringify(completeData));
				nativeInterface.earn(JSON.stringify(completeData));
				amountArray = [];
				}
		});

		$('#summary #add').on('click', function(){
			$('#modal').removeClass('summary').removeClass('active-modal');
		});

	}

}

function earnCallback(data){
	$('#modal #ok').off('click');

	$('#modal').addClass('prompt');
	$('#modal').addClass('active-modal');

	$('.icon').prepend('<img id="success" src="css/images/modal/earn.png" />')
	$('.msg').prepend('<div>'+'<h3>'+"Congratulations!"+'</h3>'+'<p>'+"You have successfully earned"+'</p>'+'<p id="value">' + data + '</p>');

	$('#prompt #ok').on('click', function(){
		$('#modal').removeClass('prompt');
		$('#modal').removeClass('active-modal');
	});
}

function showRedeem(data){

	$('#home').removeClass('active-view');
	$('#redeem').addClass('active-view');

	profile = data;

	// var profileImage = data[0].profile.image;
	var fname = data[0].profile.fname;
	var lname = data[0].profile.lname;
	var name = fname +" "+ lname;

	// if(profileImage == '' || profileImage == null){
	// 	$('#image img').attr('src','css/images/user_empty.png');
	// }else{
	// 	$('#image img').attr('src',profileImage);
	// }

	$('#name').html(name);

	var totals = '';
	totals += '<li>'+'<label>'+"Points"+'</label>'+'<div id="totalpoints">'+data[0].profile.totalPoints+'</div>'+'</li>'+
				'<li>'+'<label>'+"Total Accumulated"+'</label>'+'<div id="totalSpend">'+data[0].profile.accumulatedPoints+'</div>'+'</li>';

	$('#totals').html(totals);

	var details = '';
	details += '<li>'+'<label>'+"Card Number"+'</label>'+'<div id="cardNumber">'+data[0].profile.qrApp+'</div>'+'</li>'+
				'<li>'+'<label>'+"Phone Number"+'</label>'+'<div id="mobile">'+data[0].profile.mobileNum+'</div>'+'</li>'+
				'<li>'+'<label>'+"Email Address"+'</label>'+'<div id="email">'+data[0].profile.email+'</div>'+'</li>'+
				'<li>'+'<label>'+"Birth Date"+'</label>'+'<div id="birthdate">'+data[0].profile.dateOfBirth+'</div>'+'</li>';

	$('#details').html(details);
	// listCallback('voucher', [{"id": "18","voucherID": "VCHQK1322ZAOagD","memberID": "MEMph151513h6v4I","email": "joseph@appsolutely.ph","name": "Voucher 1-2","image": "http:\/\/124.106.33.31\/project\/saveandearn\/assets\/images\/vouchers\/full\/5886f2509fab0.png?5349","startDate": "2017-01-01","endDate": "2017-03-15","type": "registration","action": "move","status": "active","dateAdded": "2017-01-24 14:41:26","dateModified": "2017-01-24 14:41:26","description": "Voucher 12","terms": "Voucher 12"}]);
	// listCallback('voucher',[]);
	nativeInterface.getVoucherList(true);
}

function listCallback(a,data){
	$('#account').removeClass('active-tab');
	$('#accountDetails').removeClass('active-page');

	if(data != '' && data != null){

		var voucherList = "";

		for (var i = 0; i < data.length; i++) {
			voucherList+= '<li data-id="'+data[i].voucherID+'" data-name="'+data[i].name+'">'+
							'<div class="voucherName">'+data[i].name+'</div>'+
				        '</li>';
		}
		$('#voucher').addClass('active-page');
		$('#voucherList ul').html(voucherList);

	} else{
		$('#voucher').removeClass('active-page');
		$('#empty_state').addClass('active-page');
	}
}

function confirmRedeem(transID, amount){
	$('#modal #cancelBtn').off('click');
	$('#choice #submitBtn').off('click');

	$('#modal').addClass('choice');
	$('#modal').addClass('active-modal');

	$('.msg').html('Are you sure you want to redeem ' + amount + ' points?');

	$('#choice #cancelBtn').on('click', function(){
		$('#modal').removeClass('choice');
		$('#modal').removeClass('active-modal');
	});

	$('#choice #submitBtn').on('click', function(){
		$('#modal').removeClass('choice');
		$('#modal').removeClass('active-modal');
		console.log(transID, parseInt(amount).toFixed(2));
		// redeemCallback(parseInt(amount).toFixed(2)); //TEST
		nativeInterface.redeem(amount, transID);
	});
}

function redeemCallback(data,loyaltyName,redeemPoints){
	$('.msg').html('');
	$('.icon').html('');

	$('#modal #ok').off('click');

	$('#modal').addClass('prompt');
	$('#modal').addClass('active-modal');

	$('.icon').prepend('<img id="success" src="css/images/modal/redeem.png" />')
	$('.msg').prepend('<div>'+'<h3>'+"Congratulations!"+'</h3>'+'<p>'+"You have successfully redeemed"+'</p>'+'<p id="value">' + redeemPoints + ' points</p>');

	$('#prompt #ok').on('click', function(){
		$('#modal').removeClass('prompt');
		$('#modal').removeClass('active-modal');
		showRedeem(data);
	});
}

function voucherRedeem(e){

	$('#modal #cancelBtn').off('click');
	$('#choice #submitBtn').off('click');

	$('#modal').removeClass('withPoints');

	if (e.target != e.currentTarget){
		var name = e.target.getAttribute('data-name');
		var id = e.target.getAttribute('data-id');

		$('#modal').addClass('choice');
		$('#modal').addClass('active-modal');

		$('.msg').html('Are you sure you want to redeem ' + name + '?');

		$('#choice #cancelBtn').on('click', function(){
			$('#modal').removeClass('choice');
			$('#modal').removeClass('active-modal');
		});

		$('#choice #submitBtn').on('click', function(e){
			e.preventDefault();
			nativeInterface.redeemVoucher(id, name);
			e.stopPropagation();
			$('#modal').removeClass('choice');
			$('#modal').removeClass('active-modal');
		});
	}
}

function redeemVoucherCallback(voucherName){
	$('.msg').html('');
	$('.icon').html('');

	$('#modal #ok').off('click');

	$('#modal').addClass('prompt');
	$('#modal').addClass('active-modal');

	$('.icon').prepend('<img id="success" src="css/images/modal/redeem.png" />')
	$('.msg').prepend('<div>'+'<h3>'+"Congratulations!"+'</h3>'+'<p>'+"You have successfully redeemed"+'</p>'+'<p id="value">' + voucherName + '</p>');

	$('#prompt #ok').on('click', function(){
		$('#modal').removeClass('prompt');
		$('#modal').removeClass('active-modal');
	});

	nativeInterface.getVoucherList(true);
}

function errorCallback(){
	$('#voucher').removeClass('active-page');
	$('#empty_state').addClass('active-page');
}

function validateInput(){
	$('#input input').keypress(function (e) {
    var character = String.fromCharCode(e.keyCode)
        var newValue = this.value + character;
        if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
            e.preventDefault();
            return false;
        }
    });
}

function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}


















